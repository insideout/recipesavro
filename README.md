### 1. Compile Apache Avro schemas

```shell
java -cp Lib/avro-tools-1.8.2.jar \
    org.apache.avro.tool.Main compile schema \
    src/com/hsn/avro/product/Measure.avsc \
    src/com/hsn/avro/product/Carbohydrate.avsc \
    src/com/hsn/avro/product/Fat.avsc \
    src/com/hsn/avro/product/Vitamin.avsc \
    src/com/hsn/avro/product/Nutrient.avsc \
    src/com/hsn/avro/product/Product.avsc \
    src/
```

### 2. Build the artifacts

### 3. Run the application

You can take this jar wherever you want, just make sure Java 8 is installed (earlier versions of Java untested). To run the application you need a structure like this:

```
Pandora:recipes inner$ tree .
.
├── RecipesAvro.jar
└── avro
    └── products.avro

1 directory, 2 files
Pandora:recipes inner$
```

Run the application
```
java -jar RecipesAvro.jar
```

Or you can run the application straight from the IDE terminal:
```shell
java -jar out/artifacts/RecipesAvro_jar/RecipesAvro.jar
```


### 4. Result
```json
{
    "ProductNameLt": "Bolonija",
    "ProductNameEn": "Bologna",
    "ProductCategory": "Meat",
    "Nutrients": [
        {
            "ProductQty": 100.0,
            "ProductUnitOfMeasure": "g",
            "Calories": [
                {
                    "Qty": 308.0,
                    "Uom": "kcal"
                },
                {
                    "Qty": 32.0,
                    "Uom": "%"
                }
            ],
            "CaloriesFromFat": [
                {
                    "Qty": 221.0,
                    "Uom": "kcal"
                },
                {
                    "Qty": 20.0,
                    "Uom": "%"
                }
            ],
            "Fat": {
                "Saturated": [
                    {
                        "Qty": 9.301,
                        "Uom": "g"
                    }
                ],
                "Polyunsaturated": [
                    {
                        "Qty": 0.0,
                        "Uom": "g"
                    }
                ],
                "Monounsaturated": [
                    {
                        "Qty": 4.42,
                        "Uom": "g"
                    }
                ]
            },
            "Carbs": [
                {
                    "Qty": 5.49,
                    "Uom": "g"
                }
            ],
            "Protein": [
                {
                    "Qty": 15.2,
                    "Uom": "g"
                }
            ],
            "Cholesterol": [
                {
                    "Qty": 60.0,
                    "Uom": "mg"
                }
            ],
            "Sodium": [
                {
                    "Qty": 736.0,
                    "Uom": "mg"
                }
            ],
            "Potassium": [
                {
                    "Qty": 315.0,
                    "Uom": "mg"
                }
            ],
            "Carbohydrate": {
                "DietaryFiber": [
                    {
                        "Qty": 0.0,
                        "Uom": "g"
                    }
                ],
                "Sugars": [
                    {
                        "Qty": 4.42,
                        "Uom": "g"
                    }
                ]
            },
            "Vitamins": {
                "A": [
                    {
                        "Qty": 2.0,
                        "Uom": "%"
                    }
                ],
                "C": [
                    {
                        "Qty": 1.0,
                        "Uom": "%"
                    }
                ],
                "Calcium": [
                    {
                        "Qty": 8.0,
                        "Uom": "%"
                    }
                ],
                "Iron": [
                    {
                        "Qty": 7.0,
                        "Uom": "%"
                    }
                ]
            }
        }
    ]
}
```