package com.hsn.main;

import com.hsn.avro.product.Measure;
import com.hsn.avro.product.Carbohydrate;
import com.hsn.avro.product.Fat;
import com.hsn.avro.product.Vitamin;
import com.hsn.avro.product.Nutrient;
import com.hsn.avro.product.Product;

import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.*;
import org.apache.avro.file.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class App {

    private static final String avroOutputPath = "./avro/";
    private static final String avroFileName = "products.avro";

    public static void main(String[] args) throws IOException {

        // In order to create a product, need to setup dependency nutrients first
        // Create Vitamin list
        ArrayList<Measure> vitaminA = new ArrayList<>(Collections.singletonList(new Measure(2.0, "%")));
        ArrayList<Measure> vitaminC = new ArrayList<>(Collections.singletonList(new Measure(1.0, "%")));
        ArrayList<Measure> vitaminCalcium = new ArrayList<>(Collections.singletonList(new Measure(8.0, "%")));
        ArrayList<Measure> vitaminIron = new ArrayList<>(Collections.singletonList(new Measure(7.0, "%")));
        Vitamin vitamins = CreateVitamin(vitaminA, vitaminC, vitaminCalcium, vitaminIron);

        // Create Fat
        ArrayList<Measure> saturated = new ArrayList<>(Collections.singletonList(new Measure(9.301, "g")));
        ArrayList<Measure> polyunsaturated = new ArrayList<>(Collections.singletonList(new Measure(.0, "g")));
        ArrayList<Measure> monounsaturated = new ArrayList<>(Collections.singletonList(new Measure(4.42, "g")));
        Fat fats = CreateFat(saturated, polyunsaturated, monounsaturated);

        // Create CarboHydrate
        ArrayList<Measure> dietaryFiber = new ArrayList<>(Collections.singletonList(new Measure(.0, "g")));
        ArrayList<Measure> sugars = new ArrayList<>(Collections.singletonList(new Measure(4.42, "g")));
        Carbohydrate carbohydrate = CreateCarbohydrate(dietaryFiber, sugars);

        // Create Nutrient list
        ArrayList<Measure> calories = new ArrayList<>(Arrays.asList(new Measure(308.0, "kcal"), new Measure(32.0, "%")));
        ArrayList<Measure> caloriesFromFat = new ArrayList<>(Arrays.asList(new Measure(221.0, "kcal"), new Measure(20.0, "%")));
        ArrayList<Measure> carbs = new ArrayList<>(Collections.singletonList(new Measure(5.49, "g")));
        ArrayList<Measure> proteins = new ArrayList<>(Collections.singletonList(new Measure(15.2, "g")));
        ArrayList<Measure> cholesterols = new ArrayList<>(Collections.singletonList(new Measure(60.0, "mg")));
        ArrayList<Measure> sodiums = new ArrayList<>(Collections.singletonList(new Measure(736.0, "mg")));
        ArrayList<Measure> potassiums = new ArrayList<>(Collections.singletonList(new Measure(315.0, "mg")));
        Nutrient nutrients = CreateNutrient(100.0, "g", calories, caloriesFromFat, fats, carbs, proteins, cholesterols, sodiums, potassiums, carbohydrate, vitamins);

        ArrayList<Nutrient> nutrientList = new ArrayList<>();
        nutrientList.add(nutrients);

        // Create Product
        Product product = CreateProduct("Bolonija", "Bologna", "Meat", nutrientList);

        // Serialise
        DatumWriter<Product> productDatumWriter = new SpecificDatumWriter<>(Product.getClassSchema());
        DataFileWriter<Product> dataFileWriter = new DataFileWriter<>(productDatumWriter);
        dataFileWriter.create(product.getSchema(), new File(avroOutputPath + avroFileName));
        dataFileWriter.append(product);
        dataFileWriter.close();

        // Read from Avro
        ReadProducts();
    }

    private static void ReadProducts() {

        try {
            // Deserialize Products from disk
            DatumReader<Product> productDatumReader =
                    new SpecificDatumReader<>(Product.class);

            DataFileReader<Product> dataFileReader =
                    new DataFileReader<>(new File(avroOutputPath + avroFileName), productDatumReader);

            Product productList = null;
            while (dataFileReader.hasNext())
            {
                productList = dataFileReader.next(productList);
                System.out.println(productList.toString());
            }
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    private static Product CreateProduct(
            String productNameLt, String productNameEn, String productCategory, List<Nutrient> nutrients)
    {
        // Initialise new Product
        Product product = new Product(productNameLt, productNameEn, productCategory, nutrients);

        // Return
        return product;
    }

    private static Nutrient CreateNutrient(
            Double productQty,
            String productUom,
            List<Measure> calories,
            List<Measure> coloriesFromFat,
            Fat fat,
            List<Measure> carbs,
            List<Measure> proteins,
            List<Measure> cholesterols,
            List<Measure> sodiums,
            List<Measure> potassiums,
            Carbohydrate carbohydrate,
            Vitamin vitamins)
    {
        // Initialise new Nutrient
        Nutrient nutrient =
                new Nutrient(productQty, productUom, calories, coloriesFromFat, fat, carbs,
                        proteins, cholesterols, sodiums, potassiums, carbohydrate, vitamins);

        // Return
        return nutrient;
    }

    private static Fat CreateFat(List<Measure> saturated, List<Measure> polyUnsaturated, List<Measure> monoUnsaturated)
    {
        // Initialise new Fat
        Fat fat = new Fat(saturated, polyUnsaturated, monoUnsaturated);

        // Return
        return fat;
    }

    private static Carbohydrate CreateCarbohydrate(List<Measure> dietaryFiber, List<Measure> sugars)
    {
        // Initialise new Carbohydrate
        Carbohydrate carbohydrate = new Carbohydrate(dietaryFiber, sugars);

        // Return
        return carbohydrate;
    }

    private static Vitamin CreateVitamin(
            List<Measure> vitaminA, List<Measure> vitaminC, List<Measure> vitaminCalcium, List<Measure> vitaminIron)
    {
        // Create new Vitamin
        Vitamin vitamin = new Vitamin(vitaminA, vitaminC, vitaminCalcium, vitaminIron);

        // Return
        return vitamin;
    }
}
